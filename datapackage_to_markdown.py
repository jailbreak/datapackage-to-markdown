#!/usr/bin/env python3
"""
   Produce a minimal markdown file from a datapackage.json
"""
import argparse
import logging
import sys
from pathlib import Path

from datapackage import Package

log = logging.getLogger(__name__)


def output_md(package_descriptor, fdout):
    """Outputs markdown from datapackage descriptor"""

    title = package_descriptor.get('title')
    name = package_descriptor.get('name')
    heading = title or name
    if heading is not None:
        fdout.write('# {}\n\n'.format(heading))

    if heading != name:
        fdout.write("- Nom: {}\n".format(name))
    homepage = package_descriptor.get('homepage')
    if homepage is not None:
        fdout.write("- Page d'accueil : {}\n".format(homepage))
    description = package_descriptor.get('description')
    if description is not None:
        fdout.write("- Description : {}\n".format(description))
    version = package_descriptor.get('version')
    if version is not None:
        fdout.write("- Version : {}\n".format(version))
    fdout.write("\n")

    if package_descriptor.get('resources'):
        fdout.write('## Ressources\n\n')
        for res in package_descriptor.get('resources'):
            fdout.write('- [{}]({})\n'.format(res.get('name', 'title'), res.get('path')))


def main():
    """Convert datapackage.json into markdown file"""
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('datapackage_file', type=Path, help="datapackage.json file path")
    parser.add_argument('--log', default='WARNING', help='level of logging messages')
    args = parser.parse_args()

    # Log config
    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: {}'.format(args.log))
    logging.basicConfig(
        format="%(levelname)s:%(name)s:%(asctime)s:%(message)s",
        level=numeric_level,
        stream=sys.stdout,  # Use stderr if script outputs data to stdout.
    )

    datapackage_file = args.datapackage_file
    if not datapackage_file.exists():
        parser.error("Datapackage [{}] not found".format(str(datapackage_file)))

    package = Package(str(datapackage_file))

    output_md(package.descriptor, sys.stdout)


if __name__ == '__main__':
    sys.exit(main())
